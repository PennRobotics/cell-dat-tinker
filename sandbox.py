#!/usr/bin/env python3

class PortalDatViewer:
    """A searchable (eventually) catalogue (eventually) of data stored in portal.dat"""  # TODO  # TODO
    def __init__(self, filename='portal.dat'):
        with open(filename, 'rb') as portaldat_file:
            portaldat_chunk = portaldat_file.read(0x100000)
            # TODO: cleanest way to concat at end of chunk without using excess memory?
        # TODO: everything else


class CellDatViewer:
    """A parser for the map data stored in cell.dat"""
    def __init__(self, filename='cell.dat'):
        pass  # TODO


if __name__ == '__main__':
    pdv = PortalDatViewer()
    cdv = CellDatViewer()
