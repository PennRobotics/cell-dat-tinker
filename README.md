_This document refers to the video game **Asheron&rsquo;s Call** and its file structure._

## Interesting Offsets

### portal.dat

`strings -n 12 -t x portal.dat`

```text
...
4c5893f xxxx|x@@qxxxx
4c589db xxx||xxxxx{x
4c58a00 |||xv@xxxxxx
4c58a1b xxxxxxxxxzxx
4c58b00 zvvsxxv@|xxxxx|
4c58bc0 xvxxxsxx@x@@xxxx
4c58c1b xx@@@@@@sxxvxxr
4c58c4a |xx@@@@@@xxxxx
4c58c59 ~xx@@@@x@@@x
4c58c89 vxx@@@@@@@xxxxx
4c58c99 xxx@@@@xxx@s|
4c58cc8 xsxx@@@@@@@xxxxxxxx@@@@@xxxxu@sx
4c58d08 vx|@@@@@@@@xxx@@xxx@@@@@x|xx|xxx
4c58d4a |@@sx@@@@xx@@@xxx@@@@@@sxxxxxx|
4c58d8e x@@@@xx@@@xx@@@@@@@@xxvvsxv
4c58dce @@@@@xx@@@xx@@@@@@@@vvvxvvx
4c58e0d x@v@@@xx@@@xx@@@@@@@@@vvxx@x
4c58e4d @rv@@@tx@@@x@@@@@@@@s@sv@xxxxxxx@@x~
4c58e8c xrx@@@@xx@@@x@@@@@@@@vv@vr@x@@u@@sxxx
4c58ecb |@xv@@@@xx@@@x@@@@@s@@@xvvx@@@@@x
4c58f03 @sxxxv@xxxx@@@@@xx@@@x@@@@@@@@@sxv@@xx
4c58f45 xvvs@@@xv@@@@@qx@@@@@@@@@@@@@@s@@xxx
4c58f86 xx@@@@@@@@v@@@xs@@@@@@@@@@@@s@@xxxx
4c58fc6 xvx@@s@@@s@@@@xx@@@@@@@@@@@@@suxxx|x
4c59009 xxxxx@@@@s@@@@@xx@@@@@@@@@@@@@x
4c5904d x@@@@@@@@@@xx@@@@@@@s@@@@@x
4c5908e v@@@@@@@@@xx@@@@@@@@@@@@@xx
4c590ce x@s@@@@@@@xx@@@@@@@@@@s@@xx
4c5910e @@@@@@@@@@@xxx@@@@@@@@@@@@x
4c5914d x@@@@@@@@@@@xxx@@@@@@@@@@@@xx
4c5918d x@@@@@@@@@@@xxx@@@@@@@@@@@@@x
4c591cd x@@s@@@@@@@@xxx@@@@@@@@@@@@@xx
4c5920c ~@@@@@@@@@@@@xx
4c5921c @@@@@@@@@@@@@@x
.....
5fad086 Darktide-and-Half
5fad0b6 Foredawn-and-Half
5fad0e6 Dawnsong-and-Half
5fad116 Morntide-and-Half
5fad146 Midsong-and-Half
5fad176 Warmtide-and-Half
5fad1a6 Evensong-and-Half
5fad1d6 Gloaming-and-Half
...
60c2061 ~~~~~~~~~~~~~~~~~~~
60c209f |vu}xxvy}vu}}zvy}xxu}
60c20c2 ||||z|||||||||||||
60c20e2 uyyu}yuuuuuuuuuuuw
60c2102 y}}wywtuuuuuuvvvvx
60c2122 y}}wywtuuuuuuvvvvx
60c2145 ~vvuuvvvvxxxx{{
60c2162 y}}~zwuvvvxxxxxx{~
60c2187 |vvvvxxxx{{{~
60c21a2 t}}}uxvvvvxxxx{{~~
60c21c2 |}}~vvvvvvxxxz{{~~
60c21e2 y}}|~zuvvvxxx{{{~~
60c2202 y}}|}|vvvvxxxx{{{~
60c2222 t}}}uvvvvvvxxxxx{{
60c2242 t}}~xvvvxx|zxx||z{
60c2282 y}}|yzwwwwvvwxvvwz
60c22a2 y||~||||||||||||||
60c22c2 yuuuwuuuttuuuuuuuu
60c22e2 t||yyyuuuuuvvvvvxz
60c2305 }uuuuvvvxx{{{{~
60c2325 }uuuuvvvxx{{{{~
60c2345 }vvuvvvxxx{{{~~
60c2365 }}zuvvvxxxx{{~~
60c2385 ~yzvvvvxxxx{{~~
60c23a6 uvvvvvvvxxxx{~
60c23e2 t}}|}|uvxx}yvx}}y{
60c2406 t}}~uzzzzzwzzzzzzz
60c2426 u||}yz||||||||||||
60c2446 twwuywttuuuuuuuuuu
60c2469 |yyuuvvvvxxxxx{
60c248a uuuvvvxxxx{{{{
60c24a9 }wvuvvvxxxx{{{~
60c24c9 |}|vvxxxxxx{{{~
60c24e9 |uzvvvvxxxx{{{{
60c2509 }vvvvvvvvxxxx{{
60c2529 }vvvvvvvvxxxx{{t
60c2556 ~{uutttttttttt
60c2569 z}|uvxx|wvxzzy{xzwwwwvuutttt
60c2589 |uzzzzzzzzzzzz|wwwwwwwzwuuuuutyy|wwyyyyyyywwwyywwwwwwwwzzzwuutwwwyytttttuuuuuuuuuuuuuuwwzzzutt~~}yyuuuuvvxxxx{{vwuuuuuuvwwzwtt
60c260a uuuuvvvxxx{{{{wwwwwwwwwwwwwtt
60c2629 ~zwuvvvxxxx{{{{wwwwwwwwwwwwutt
60c2649 }}}uuvvvxxx{{{{wuuuuuuuuvvuttt
60c2669 ~uxvuvvxxx{{{{~uutttttuuuuuttt
60c268a vvvvvvvxx{xx{~wwwwwwwwwwwwwtt}}~
60c26ab |vvxx|zxx~~~{wwwwzzzwwwwwutt}}|}zvvxx
...
616842a Nanto Allegiance Hall
616844a Qalabar Allegiance Hall
616846e Rithwic Allegiance Hall
6168492 Samsur Allegiance Hall
61684b6 Sawato Allegiance Hall
61684da Shoushi Allegiance Hall
61684fe Tou-Tou Allegiance Hall
6168522 Tufa Allegiance Hall
6168542 Uziz Allegiance Hall
6168562 Yanshi Allegiance Hall
6168586 Yaraq Allegiance Hall
61685a6 Zaikhal Allegiance Hall
61685ca Dungeon Portals
...
6229b42 Tumerok Officer
6229b5e Tumerok Officer Archer
6229b82 Tumerok Gladiator
6229b9e Tumerok Gladiator Archer
6229bc2 Tumerok Overlord
6229bf2 Female Tusker
622ccc6 Blade Vulnerability Other II
622ccee Blade Vulnerability Other III
622cd16 Bludgeon Protection Other I
622cd3e Bludgeon Protection Other II
622cd66 Bludgeon Protection Other III
622cd8e Bludgeon Protection Self I
622cdb6 Bludgeon Protection Self II
622cdde Bludgeon Protection Self III
622ce06 Bludgeon Vulnerability Other I
...
6231c66 Item Expertise Self VI
6231c8a Item Ignorance Other Iv
6231cae Item Ignorance Other V
6231cd2 Item Ignorance Other VI
...
62361a2 QuarterStaff
6236286 Missle Weapons
62362e2 Crossbow- Heavy
62362fe Crossbow- Light
623631a Throwing Axe
6236332 Throwing Club
62363ca Weapons- Magic
...
623743e Aluvian Door- Activated
6237462 Dungeon Door- Activated
6237486 Dungeon Door- Activated Fast
62374ae Gharundium Door- Activated
62374d6 Prison Door Activated
62374f6 Sho Left Door- Activated
623751a Sho Right Door- Activated
623753e Sho Sliding Door- Activated
6237566 Olthoi Door -Activated
623758a Olthoi Door -Activated Fast
62375b2 Metal Cave Door -Activated
62375da Metal Cave Door -Activated Fast
6237606 Wood Cave Door -Activated
623762a Wood Cave Door -Activated Fast
6237656 Openable by AIs
6237672 Dungeon Door
62376a2 Cave Door (Metal)
62376be Cave Door (Wooden)
62376de Aluvian House Door
62376fe Gharundim House Door
623771e Sho Door (swings left)
6237742 Sho Door (swings right)
6237766 Sho Door (slides open)
62377b6 Arwic Vendors
62377ce Arwic Archmage
62377ea Arwic Barkeeper 1
6237826 Arwic Blacksmith 1
6237846 Arwic Blacksmith 2
6237866 Arwic Bowyer
...
6336436 PhyntosWasps
633644e Red PhyntosWasp Generator
6336472 Green PhyntosWasp Generator
633649a Blue PhyntosWasp Generator
63364d2 White Rat Generator
63364f2 Grey Rat Generator
6336512 Brown Rat Generator
6336532 Black Rat Generator
6336552 Red Rat Generator
6336586 Reedshark Pup Generator
63365aa Adult ReedShark Generator
63365ce Reedshark Veteran Generator
633660a Tusker Male Generator
633662a Tusker Female Generator
633664e Tusker Goldenback Generator
633668a Zombie Undead Generator
63366ae Zombie Generator
63366ca Zombie Lich Generator
63366ea Zombie Revenant Generator
633670e Undead- Flaming Helm Gen
6336746 Virindi Servant Generator
633676a Treasure! (Basic)
...
633ac5a Armor Other II
633ac76 Armor Other III
633ac92 Armor Self I
633acaa Armor Self II
633acc2 Armor Self III
633acde Bafflement Other I
633acfe Bafflement Other II
633ad1e Bafflement Other III
633ad3e Clumsiness Other I
633ad5e Clumsiness Other II
633ad7e Clumsiness Other III
633ad9e Coordination Other I
633adbe Coordination Other II
...
d2b7426 ////.///0001111010110000//001000011100010//.....//0100111011000/0000/01110111100///////.//
d2b74a6 /////0000112122111100////01101010010010000///.///00010111011100000//000110112211000000//0/
d2b7526 /011112222221000111000/0001001111010///000//////////0/010121100000000001110001122222211110
d2b75a6 22424424442211110211110001111111110/..///00/00/0/0/..//00111111001111112222101222242244222
d2b7626 244444444212455521111111000/000/00/.../////0000/00//.///00/0000010112221245554222244444444
d2b76a6 44444444444445542222221110//////0//////000/0000/000/////////////00111222244555424444444444
d2b7726 45444454444444421222211111000//////////11/0000010000//0/////000011011121124224444455554445
d2b77a6 5555445444442242222221000111110/////0000100111101000000////0111100112211224444444445554555
d2b782a 55555554444444422422111111111100/00000100110101000000/000001111111112212244444444445555555
d2b78aa 555655555544444442222222211111110000000001110011001100000101112122222222244444455555566555
d2b792a 555566665554544444222222112211110000001011010011110111000101112122222222445554455555555555
d2b79aa 555555665555555555544422222221101001111010011000011111011111111222224444555555555656555555
d2b7a2a 555555666655555554554442222222111000111000001101000010000112222222224455555555556665555555
d2b7aaa 5455555666655555555444422222222110101001111001100///01000012422222424445555555566665555454
d2b7b2a 24555555666555555554442444242221110000000101100000//00001122242424444445555555666655555544
d2b7baa 2455555555565555544200/04555424220000000000110000////00001244455420/012455555556565555
d2b7c2e 555555555555555555551./14565422210///////01111000//..//001244466540./454545555555555555544
d2b7cae 665565554555555555555445554422110////00000010110//110/./0112224555544555545545455445665566
d2b7d2e 66665544445555554442242242211100/../1440/001110//04540.//001111222222244455555555454555666
d2b7dae 454444444555555544211010112100///../1221///0000//0442/..////011111001124445555554544444454
d2b7e2e 1122244456666555444442221110//.......//00/000/0///00/......//00111224244455556665554442111
d2b7eae 001224445666665442222111110////.......//////000////........///0011112224245666665444442111
d2b7f2e 00111222445555422210110010///............////00///...........//00001111222445555442222100/
d2b7fae ///0000111111111110000000//..---.....-......///...........---.///00000011111111111
d2b8032 ........//0001000000000////.------.----.................------..///000011000000///...../..
d2b80b2 -.---...../0/0////////........-++,------..../.......----,++-........///0000/0//......---.
d2b8133 ,,-.00/....//././..............-,,-------.......-.----,,-............../.../...../00.-,
d2b81b5 .1554/..........------..---/21.-,-------....---------/21.--..------.........-.04541.
d2b8237 02444/.--....--,,,+,-----.11/---------.....-------.120.-----,,,,,--....--.045441-
d2b82b9 ,..-,,,,--,+,./...------.//.-,------------------...-------..//-++,--,,,,./.-,+
d2b833b *+++,,,,+,-142/.-++,-,--------------------------,,,-,,,-.044/-,+,,,+,+++++*
d2b83bd ***+++,.04441.+**++,,+,,,,,,,,-----------,,,,,,,,+***,.2441/-++++++
d2b8443 ****,-/122.+*))**++++++,,,,,--------,,,,,,++++**)))*-0220.-,**+*****
d2b84c5 )*+-..-+*))))))**+++++++,,,-------,,,,+++++**)))))*+,-..-+*******
d2b8548 )*)))))))))******+++++++,,-----,,++********)))))))**********
d2b85cc (()))))**********+++,,----,,++*********))))))))))))))*
d2b864e ((((())))*******++,,---,,,+******)))))(((((())))(
d2b86d2 (((()*++*****++,,--,,+***+,+*)((((((((((()
d2b8756 *+,+*))))*+,--,,+*****++*((((((((((
d2b87da *)))))*+,,++**)))))))(()))))
d2be4b9 )))*+++,-,,,++******))
d2be537 )))**++,,---,,,+**********)
d2be5b5 ))***+++,,----,,,+++**********)
d2be633 )***+++++,,------,,,+++++++****))))
d2be6b2 )**++++,,,,,-------,,,,,,,+++++*))))*,
d2be72f )))*+++,+,,,,,,------,,,,,,,,+,++++*))*,042
d2be7ac --,+*+,,,,,,,,-------------,,,,,,,,,,,+++,-0244.
d2be82f 0/.-,,------.-------------------,-.--------..02/-,+
d2be8ad +----------.00.--------...--------,./0/.--------.-,,--,
d2be92b ,---,,,,-..--021.--------....---------.02/-----,,,---.....
d2be9a9 +-...-....-..-.//.--------......-------,-.0/.-............./.
d2bea28 -.//............-++,---.-.........-------,+,-...........///.///
d2beaa7 /00000/0//./..---,+,--........./....-------,,---..//////00/0000013
d2beb25 -/11110000010//..----.....-.....//......--....-,--..//00000001110000.
d2beb9e 11100012222211111000///...-...........////............--..//0101111112222222211
d2bec21 24444556665422222111000////........//./0000//../.........///0011122222455665554441
d2bec9f 12244456666665442242221100///........////0100///0/........//0001122444445566666554441
d2bed1d 12222444555655555444222211110//....../11///00000//1000.....//011121122244455556555544441
d2bed9d 5655544554455555542221100122000////./4541//00000//1442/..///01221100111244555554455555565
d2bee1d 665566554555555544444555554111100//./1421//111110/0221///000122245455444555555555455666566
d2bee9d 65555555555555554555510255654422100///////0111110///////0112244555420455555555555555555556
d2bef1d 245555555655555555542/./25654442110///////00111000/////0/1224455540..024455555565555555544
d2bef9d 245555555665555555542211244444421100000//0001111100000000124444442212224455555565655555542
d2bf021 44554555666655555554444444424422110000///0001111110000001012244244224445555555566665555544
d2bf0a1 45555556666555555544444222222422100001000/110000110010001012242122224445555555556666555555
d2bf121 455555666655555554554442222222111101110000110000001000011112122222224455555555556666555545
d2bf1a1 555556666555555544444422222221100100010000001000111101000011111212122444445555555656555555
d2bf221 555566555544555444222222221211110010000100000111010110010011121221212222224444554555566555
d2bf2a1 555555555544444424222211122111110010001000000111100000000011111212222222222444445555566555
d2bf321 5554555444444244222211110111111000/000001000111100000///0/00111111101222222444444445555555
d2bf3a1 4544544454442424222121110011110////////01100101100000/0////0011100111112212242444445544455
d2bf425 44444544444444442221211110000//////////01000//0000000////////00000011112222444444444455445
...
```

Notably, spell words do not appear here, so it's clear the client executable also should contain a significant database of resources _or_ there is some type of compression/encryption. Since most strings are not obscured and file sizes are rather large, I expect there is no compression.

### cell.dat

Running the same command, there are no recognizable strings. However, there are a lot of ``T`T`T`T`T`T`T`T`T`T`T`T`T`T`` and ``P`P`P`P`P`P`P`P`P`P`P`P`` as well as nearby letter sequences like `OPPONKHD` or `RNOPQRRRQONNOPQPONLLMMNNMLJ`. This suggests to me the entire file might be a giant 2d array.

Dereth is supposed to be 24 miles by 24 miles. If there is no metadata, this file size should then be a multiple of 576 and essentially a squared value.

```text
> du -b cell.dat
207618048       cell.dat
```

We can see that $`\sqrt{207618048} \approx 14408.96`$ and 14400 would be 24 times 600.

How many extra bytes exist? What is their purpose?

To find out, we see that the file is oversized by 258,048 bytes. This is almost 508 squared, but that does not ring an immediate bell. The prime factors are $`2^{12} \cdot 3^{2} \cdot 7`$ or 24 times 10752.

Whatever. Perhaps the best strategy is to see how each line aligns when split at byte boundaries near 14400?

When running `od -tx1 -v -N1440000 -w14400 cell.dat` there are too many lines in the middle that are `00 00 00 00 00 00 00 00` surrounded by lines with nonzero content. Still, it seems this is the right track.

Notably, a lot of lines are `... 01 xx 01 yy 01 zz 01 ...`. Maybe these are 16-bit blocks? That would then break the square root rule above, so maybe two 16-bit blocks define each region?

Then `od -tx2 -v -N1440000 -w7200 cell.dat` is tried:

```text
0000000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 0000 ...
0016040 0050 0050 0050 0050 0050 0050 0050 0050 0050 0050 0050 0050 0050 ...
0034100 01d3 0000 0001 0015 01ca 0001 01ca 01d3 01cd 01d4 01d0 01cc 01d1 ...
0070200 01e2 01e8 0208 01fb 01ec 0204 0306 ...
...
```

Okay. Where does 0050 start? Column 6430, which is byte 2572. Things right before that are `00fc 0000 ffff 0122 4d00 0001 00fc 0000 0000 0000 ffff 0002 0000 0000` which could be indices or some other encoding.

There are definitely patterns at various outputs. Notably, `od -tx4 -v -N1440000 -w512 cell.dat > cdsnipp.txt` has a lot of seemingly vertical alignment.

## Random Links

- https://github.com/ACEmulator/ACViewer
- https://github.com/harliq/Weenie-Fab
- https://github.com/GoTBrainstorm/acrender
- https://github.com/mcejp/AC-Resources
- https://old.reddit.com/r/AsheronsCall/comments/5kpbgo/first_batch_utilities_and_dat_files/
- https://old.reddit.com/r/AsheronsCall/comments/5ji48x/object_viewers_celldat_viewers_emulators/
- https://github.com/amoeba/dereth-cartography
- map
    - https://www.reddit.com/r/AsheronsCall/comments/yoziuq/i_found_a_high_res_map_of_dereth_from_2004/
    - https://asheron-files.s3.amazonaws.com/map/editor1.jpg
    - https://asheron-files.s3.amazonaws.com/map/cod_20060806.xml
- https://asheron.fandom.com/wiki/Terragen
- https://aka-steve.com/i-want-updates/
- https://www.gamedeveloper.com/design/classic-postmortem-i-asheron-s-call-i-
    - _World Builder_
- https://www.gamedeveloper.com/programming/the-tools-development-of-turbine-s-i-asheron-s-call-2-i-
- https://github.com/nygard/PatchAC2Data/blob/master/patch-ac2-data.py
- ** http://stevenygard.com/download/ac/ **

-----

- [ ] possibility of fully determining the interaction between the client executable and cell.dat for the earliest releases?
- [ ] what would it take to make a completely new cell.dat?
- [ ] is there a reliable way to determine byte-level read accesses to cell.dat (akin to code coverage)?
- [ ] use file entropy and custom parser to find interesting data segments
    - [ ] try finding 2d arrays
    - [ ] create memory map with major regions highlighted